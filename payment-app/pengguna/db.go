package pengguna

type penggunaDataModel struct {
	IDPengguna    int64
	NamaPengguna  string
	NomorTelepon  string
	Alamat        string
	Email         string
	Balance       float32
	URLFotoProfil string
}
