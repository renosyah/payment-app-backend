package transaksi

import (
	context "context"
	"database/sql"
	"fmt"
)

type transaksiDataModel struct {
	IDTransaksi    int64
	IDPengirim     int64
	IDPenerima     int64
	Nominal        float32
	WaktuTransaksi string
}

func (t *TransaksiData) addTransaksi(ctx context.Context, db *sql.DB) (int64, error) {
	var id int64 = 0
	query := `INSERT INTO payment_db.Transaksi (
		id_pengirim,
		id_penerima,
		nominal,
		waktu_transaksi) VALUES ($1,$2,$3,$4) RETURNING id_transaksi`

	//query2 := `UPDATE payment_db.Pengguna SET balance = balance+'$3' WHERE id_pengguna = '$2'`
	err := db.QueryRowContext(ctx, fmt.Sprintf(query), t.IdPengirim,
		t.IdPenerima, t.Nominal, t.WaktuTransaksi).Scan(&id)
	if err != nil {
		return id, err
	}
	return id, nil
}

func (t *transaksiDataModel) getAllTransaksi(ctx context.Context, db *sql.DB) ([]TransaksiData, error) {
	transaksiDatas := []TransaksiData{}
	query := `SELECT id_transaksi, id_pengirim, id_penerima, nominal, 
		waktu_transaksi FROM payment_db.Transaksi 
		WHERE id_pengirim = $1`
	rows, err := db.QueryContext(ctx, fmt.Sprintf(query), t.IDPengirim, t.IDPenerima)
	if err != nil {
		return transaksiDatas, err
	}
	for rows.Next() {
		transaksiData := TransaksiData{}
		rows.Scan(&transaksiData.IdTransaksi, &transaksiData.IdPengirim,
			&transaksiData.IdPenerima, &transaksiData.Nominal, &transaksiData.WaktuTransaksi)
		transaksiDatas = append(transaksiDatas, transaksiData)
	}
	rows.Close()
	return transaksiDatas, nil
}
